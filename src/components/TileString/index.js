import React from 'react';
import { Card, CardHeader, CardBody, CardFooter } from 'reactstrap';
import axios from 'axios';
import classNames from 'classnames';
import tile from '../../common/tile.js';
import process from '../../common/process.js';
import moment from 'moment';
import posed from 'react-pose';
import { get } from 'lodash';

//--------------------------------------------------------//
//	Pose
//--------------------------------------------------------//
const Box = posed.div(tile.pose());

//--------------------------------------------------------//
//	Component
//--------------------------------------------------------//
export default class TileString extends React.Component {

	//	Default Props
	//--------------------------------------------------------//
	static defaultProps = {
		type: 'string',
		subtype: 'basic',
		x: 0, y: 0, w: 1, h: 1,
		entities: [],
		label: '',
		meta: '',
		valueType: 'COMMON',
		valueMatch: 'off',
		metaType: 'STRING',
		ajaxInterval: 0,
		icons: {},
		colors: {},
		ws: {}
	};

	//	Constructor
	//--------------------------------------------------------//
	constructor(props, defaultProps) {
		super(props, defaultProps);
		this.state = {
			isLoading: false,
			isLoaded: false,
			hasLoaded: false,
			isError: false,
			hasErrored: false,
			rawData: {},
			entityValues: [],
			value: '',
			oldValue: '',
			icon: false,
			iconClasses: '',
			color: false,
			colorClasses: ''
		};
	}

	//	Handlers
	//--------------------------------------------------------//
	timerID;

	//	Lifecycle
	//--------------------------------------------------------//
	componentDidMount() {
		this.ajaxLoad();

		//	Ajax Refresh
		//--------------------------------------------------------//
		if(this.props.ajaxInterval > 0) {
			this.timerID = setInterval(() => this.ajaxLoad(), this.props.ajaxInterval * 1000);
		}
	}

	componentWillUnmount() { clearInterval(this.timerID); }

	static getDerivedStateFromProps(props, state) {
		if(get(props, 'ws.event.event_type', '') === 'state_changed' && props.entities.indexOf(props.ws.event.data.entity_id) > -1) {
			return { rawData: props.ws.event.data.new_state };
		} else { return null; }
	}

	componentDidUpdate(props, state) {
		if(get(state, 'rawData.state', '') === '') { return; }
		else { this.update(get(state, 'rawData')); }
	}

	//	Classes
	//--------------------------------------------------------//
	tileClasses = classNames(tile.classes(this.props));

	//	Update
	//--------------------------------------------------------//
	update = (data) => {

		//	Setup
		//--------------------------------------------------------//
		let label = this.state.label;
		let meta = this.state.meta;
		let entityValues = this.state.entityValues;
		const oldValue = this.state.value;
		const entityIndex = this.props.entities.indexOf(data.entity_id);

		//	Label & Meta
		//--------------------------------------------------------//
		if(label === undefined) { label = get(data, 'attributes.friendly_name'); }
		switch(this.props.metaType) {
			case 'UPDATED':
				meta = moment(get(data, 'last_changed')).format('Do MMM HH:mm');
				break;
			default:
				meta = meta ? meta : '';
				break;
		}

		//	Value
		//--------------------------------------------------------//
		entityValues[entityIndex] = data.state;

		//	Processed Value
		//--------------------------------------------------------//
		const value = process.string(entityValues, this.props);
		const icon = process.icon(value, this.props);

		let iconClasses = '';
		if(icon) {
			iconClasses = classNames({
				'mdi': true,
				[`mdi-${icon}`]: true
			});
		}

		const color = process.color(value, this.props);
		const colorClasses = classNames({
			'color': (color !== false),
			'color--vanilla': (color === false),
			[`color--${color}`]: true
		});

		//	Save Update
		//--------------------------------------------------------//
		if(value !== oldValue) {
			this.setState({
				rawData: {},
				label: label,
				meta: meta,
				value: value,
				oldValue: oldValue,
				icon: icon,
				iconClasses: iconClasses,
				color: color,
				colorClasses: colorClasses,
				justUpdated: true
			});

			//	Back To Normal
			//--------------------------------------------------------//
			setTimeout(() => {
				this.setState({justUpdated: false});
			}, 500);
		}
	}

	//	Ajax Load
	//--------------------------------------------------------//
	ajaxLoad = async (e) => {
		if(e !== undefined) { e.preventDefault(); }
		this.setState({
			isLoading: true,
			isLoaded: false
		});
		try {
			await Promise.all(this.props.entities.map(async (entity, entityIndex) => {
				const res = await axios.get('/api/states/'+entity);
				this.update(res.data);
			}));
			this.setState({
				isLoading: false,
				isLoaded: true,
				hasLoaded: true,
				isError: false
			});
		} catch(error) {
			this.setState({
				isLoading: false,
				isLoaded: false,
				isError: true,
				hasErrored: true
			});
			console.error(error);
		}
	}

	//	Render
	//--------------------------------------------------------//
	render() {
		return (
			<Box className={this.tileClasses} {...this.props} pose={this.state.justUpdated ? 'justUpdated' : 'normal'}>
				<Card>
					<CardHeader>{this.props.meta || this.state.meta}</CardHeader>
					<CardBody className={this.state.colorClasses}>
						{ this.state.icon ? (
							<i className={this.state.iconClasses}></i>
						) : (
							<span classNames="string">{this.state.value}</span>
						)}
					</CardBody>
					<CardFooter>
						<span className="label">{this.props.label || this.state.label}</span>
						{ this.props.entities.length > 1 ? (
								<span className="badge badge-secondary">{this.props.entities.length}</span>
							) : null
						}
					</CardFooter>
				</Card>
			</Box>
	    );
	}
}
