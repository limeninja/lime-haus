import { countBy, get } from 'lodash';
import chroma from 'chroma-js';

export default {

	//--------------------------------------------------------//
	//	To Boolean
	//--------------------------------------------------------//
	toBool: (value) => {
		switch(value) {
			case 'off':
				return false;
			case 'on':
			case 'MIXED':
			default:
				return true;
		}
	},

	//--------------------------------------------------------//
	//	To State
	//--------------------------------------------------------//
	toState: (value) => {
		return value ? 'on' : 'off';
	},

	//--------------------------------------------------------//
	//	Sum
	//--------------------------------------------------------//
	sum: (values) => {
		return Math.round(values.reduce(function (sum, val) { return sum + val }, 0) * 10) / 10;
	},

	//--------------------------------------------------------//
	//	Numbers (1D)
	//--------------------------------------------------------//
	numbers: (values, props) => {
		if(values.length === 1) { return values[0]; }
		switch(props.valueType) {
			case 'AVG':
				return values.reduce( ( p, c ) => p + c, 0 ) / values.length;
			case 'SUM':
				return values.reduce(function (sum, val) { return sum + val }, 0);
			case 'LO':
				return Math.min(...values);
			case 'HI':
				return Math.max(...values);
			default:
				return values[0];
		}
	},

	//--------------------------------------------------------//
	//	String
	//--------------------------------------------------------//
	string: (values, props) => {
		if(values.length === 1) { return values[0]; }
		const counts = countBy(values);
		switch(props.valueType) {
			case 'COMMON':
				let common = {
					count: 0,
					value: values[0]
				};
				for(const [value, count] of Object.entries(counts)) {
					if(count > common.count) {
 					   common = { count: count, value: value };
 				   }
				}
				return common.value;
			case 'RARE':
				let rarest = {
					count: 0,
					value: values[0]
				};
				for(const [value, count] of Object.entries(counts)) {
					if(count < rarest.count || rarest.count === 0) {
 					   rarest = { count: count, value: value };
 				   }
				}
				return rarest.value;
			case 'MIXED':
				if(Object.entries(counts).length === 1) { return values[0]; }
				else { return 'MIXED';  }
			case 'ANYOF':
				if(values.indexOf(props.valueMatch) > -1) { return 'TRUE'; }
				else { return 'FALSE'; }
			default:
				return values[0];
		}
	},

	//--------------------------------------------------------//
	//	Icon
	//--------------------------------------------------------//
	icon: (value, props) => {
		let result = false;
		if(get(props, 'icons', []).length) {
			for(const icon of get(props, 'icons')) {
				if(icon.eq !== undefined && value === icon.eq) { result = icon.icon; }
				else if(icon.gt !== undefined && value > icon.gt) { result = icon.icon; }
				else if(icon.gte !== undefined && value >= icon.gte) { result = icon.icon; }
				else if(icon.lte !== undefined && value <= icon.lte) { result = icon.icon; }
				else if(icon.lt !== undefined && value < icon.lt) { result = icon.icon; }
			}
		}
		return result;
	},

	//--------------------------------------------------------//
	//	Color
	//--------------------------------------------------------//
	color: (value, props) => {
		let result = false;
		if(get(props, 'colors', []).length) {
			for(const color of get(props, 'colors')) {
				if(color.eq !== undefined && value === color.eq) { result = color.color; }
				else if(color.gt !== undefined && value > color.gt) { result = color.color; }
				else if(color.gte !== undefined && value >= color.gte) { result = color.color; }
				else if(color.lte !== undefined && value <= color.lte) { result = color.color; }
				else if(color.lt !== undefined && value < color.lt) { result = color.color; }
			}
		}
		return result;
	},

	//--------------------------------------------------------//
	//	Color Scale
	//--------------------------------------------------------//
	colorRange: (value, props) => {
		let result = false;
		if(get(props, 'colorRange', '') !== '') {
			let scale;
			const rootStyle = getComputedStyle(document.body);
			switch(props.colorRange) {
				case 'TEMP':
					scale = chroma.scale([
						rootStyle.getPropertyValue('--c-scale-temp-0'),
						rootStyle.getPropertyValue('--c-scale-temp-1'),
						rootStyle.getPropertyValue('--c-scale-temp-2'),
						rootStyle.getPropertyValue('--c-scale-temp-3'),
						rootStyle.getPropertyValue('--c-scale-temp-4')
					]);
					break;
				case 'BADGOOD':
					scale = chroma.scale([
						rootStyle.getPropertyValue('--c-scale-badgood-0'),
						rootStyle.getPropertyValue('--c-scale-badgood-1'),
						rootStyle.getPropertyValue('--c-scale-badgood-2')
					]);
					break;
				case 'GOODBAD':
				default:
					scale = chroma.scale([
						rootStyle.getPropertyValue('--c-scale-goodbad-0'),
						rootStyle.getPropertyValue('--c-scale-goodbad-1'),
						rootStyle.getPropertyValue('--c-scale-goodbad-2')
					]);
					break;
			}
			result = scale(value / 100).hex();
		}
		return result;
	}
}
