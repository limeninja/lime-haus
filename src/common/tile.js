export default {

	//--------------------------------------------------------//
	//	Classes
	//--------------------------------------------------------//
	classes: (props) => {
		return {
			'tile': true,
			[`tile--${props.type}`]: true,
			[`subtype--${props.subtype}`]: true,
			[`tile--x${props.x}`]: true,
			[`tile--y${props.y}`]: true,
			[`tile--w${props.w}`]: true,
			[`tile--h${props.h}`]: true
		}
	},

	//--------------------------------------------------------//
	//	Pose
	//--------------------------------------------------------//
	pose: (props) => {
		return {
			justUpdated: {
				scale: 0.9,
				opacity: 0.7,
				transition: {
					type: 'spring',
					stiffness: 200,
					damping: 0
				}
			},
			normal: {
				scale: 1,
				opacity: 1
			}
		}
	}
}
