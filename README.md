# Lime.Haus
A lovelace alternative dashboard for Hass.io (and core Home Assistant).


## Stat
```yaml
views:
  - name: 'My View'
    tiles:
      - type: 'stat'
        x: 4
        y: 0
        label: 'Socket'
        entities:
          - 'sensor.test_socket_watts'
```

| Prop        | Default Value                      | Options                          | Notes                  |
| ----------- |:-----------------------------------|:---------------------------------|:-----------------------|
| x           | 0                                  | `Number`                         |                        |
| y           | 0                                  | `Number`                         |                        |
| label       | (first entity friendly name)       | `String`                         |                        |
| meta        | (first entity unit of measurement) | `String`                         |                        |
| entities    |                                    | `Array` of entity IDs            |                        |
| interval    | 30                                 | `Number`                         | in SECONDS             |
| valueType   | `'AVG'`                            | `'AVG'`, `'SUM'`, `'LO'`, `'HI'` |                        |
