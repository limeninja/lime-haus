import React from 'react';
import { Card, CardHeader, CardBody, CardFooter } from 'reactstrap';
import CircularProgressbar from 'react-circular-progressbar';
import { Doughnut } from 'react-chartjs-2';
import CountUp from 'react-countup';
import axios from 'axios';
import classNames from 'classnames';
import tile from '../../common/tile.js';
import graph from '../../common/graph.js';
import process from '../../common/process.js';
import moment from 'moment';
import posed from 'react-pose';
import { get, cloneDeep, round } from 'lodash';

//--------------------------------------------------------//
//	Pose
//--------------------------------------------------------//
const Box = posed.div(tile.pose());

//--------------------------------------------------------//
//	Component
//--------------------------------------------------------//
export default class TileNumber extends React.Component {

	//	Default Props
	//--------------------------------------------------------//
	static defaultProps = {
		type: 'number',
		subtype: 'basic',
		x: 0, y: 0, w: 1, h: 1,
		entities: [],
		label: '',
		meta: '',
		precision: 1,
		valueType: 'SUM',
		metaType: 'STRING',
		ajaxInterval: 0,
		range: [0, 100],
		icons: {},
		colors: {},
		colorRange: '',
		percentWidth: 10,
		ws: {}
	};

	//	Constructor
	//--------------------------------------------------------//
	constructor(props, defaultProps) {
		super(props, defaultProps);
		this.state = {
			isLoading: false,
			isLoaded: false,
			hasLoaded: false,
			isError: false,
			hasErrored: false,
			rawData: {},
			entityValues: [],
			value: 0,
			oldValue: 0,
			percentValue: 0,
			gaugeData: {},
			icon: false,
			iconClasses: '',
			color: false,
			colorClasses: '',
			scaleColor: false,
			inlineStyle: {}
		};

		//	Chart Setup
		//--------------------------------------------------------//
		this.graphOptions = graph.gaugeOptions(props);
		this.seedDataset = graph.gaugeDataset(props);
	}

	//	Handlers
	//--------------------------------------------------------//
	timerID;

	//	Lifecycle
	//--------------------------------------------------------//
	componentDidMount() {
		this.ajaxLoad();

		//	Ajax Refresh
		//--------------------------------------------------------//
		if(this.props.ajaxInterval > 0) {
			this.timerID = setInterval(() => this.ajaxLoad(), this.props.ajaxInterval * 1000);
		}
	}

	componentWillUnmount() { clearInterval(this.timerID); }

	static getDerivedStateFromProps(props, state) {
		if(get(props, 'ws.event.event_type', '') === 'state_changed' && props.entities.indexOf(props.ws.event.data.entity_id) > -1) {
			return { rawData: props.ws.event.data.new_state };
		} else { return null; }
	}

	componentDidUpdate(props, state) {
		if(get(state, 'rawData.state', '') === '') { return; }
		else { this.update(get(state, 'rawData')); }
	}

	//	Classes
	//--------------------------------------------------------//
	tileClasses = classNames(tile.classes(this.props));

	//	Root Style
	//--------------------------------------------------------//
	rootStyle = getComputedStyle(document.body);

	//	Update
	//--------------------------------------------------------//
	update = (data) => {

		//	Setup
		//--------------------------------------------------------//
		let label = this.state.label;
		let meta = this.state.meta;
		let entityValues = this.state.entityValues;
		const oldValue = this.state.value;
		const entityIndex = this.props.entities.indexOf(data.entity_id);

		//	Label & Meta
		//--------------------------------------------------------//
		if(label === undefined) { label = get(data, 'attributes.friendly_name'); }
		switch(this.props.metaType) {
			case 'UPDATED':
				meta = moment(get(data, 'last_changed')).format('Do MMM HH:mm');
				break;
			default:
				meta = meta ? meta : get(data, 'attributes.unit_of_measurement');
				break;
		}

		//	Value
		//--------------------------------------------------------//
		const tempValue = parseFloat(data.state);
		if(!isNaN(tempValue)) { entityValues[entityIndex] = tempValue; }

		//	Processed Value
		//--------------------------------------------------------//
		const value = round(process.numbers(entityValues, this.props), this.props.precision);
		const percentValue = round(((value - this.props.range[0]) * 100) / (this.props.range[1] - this.props.range[0]), this.props.precision);

		const icon = process.icon(value, this.props);
		let iconClasses = '';
		if(icon) {
			iconClasses = classNames({
				'mdi': true,
				[`mdi-${icon}`]: true
			});
		}

		const color = process.color(value, this.props);
		const colorClasses = classNames({
			'color': (color !== false),
			'color--vanilla': (color === false),
			[`color--${color}`]: true
		});

		const scaleColor = process.colorRange(percentValue, this.props);
		let inlineStyle = {};
		if(scaleColor) {
			inlineStyle = {
				color: scaleColor
			};
		}

		//	Save Update
		//--------------------------------------------------------//
		if(value !== oldValue) {

			//	Gauge Value
			//--------------------------------------------------------//
			let gaugeData = { datasets: [] };
			let datasetObject = cloneDeep(this.seedDataset);
			datasetObject.data.push(percentValue);
			datasetObject.data.push(Math.max(100 - percentValue, 0));
			gaugeData.datasets.push(datasetObject);

			let colors;
			if(scaleColor) {
				colors = [
					scaleColor,
					this.rootStyle.getPropertyValue('--c-background')
				];
			} else if(color) {
				colors = [
					this.rootStyle.getPropertyValue(`--${color}`),
					this.rootStyle.getPropertyValue('--c-background')
				];
			} else {
				colors = [
					this.rootStyle.getPropertyValue('--c-1'),
					this.rootStyle.getPropertyValue('--c-background')
				];
			}
			datasetObject.backgroundColor = colors;

			//	Save Update
			//--------------------------------------------------------//
			this.setState({
				rawData: {},
				label: label,
				meta: meta,
				value: value,
				oldValue: oldValue,
				percentValue: percentValue,
				gaugeData: gaugeData,
				icon: icon,
				iconClasses: iconClasses,
				color: color,
				colorClasses: colorClasses,
				scaleColor: scaleColor,
				inlineStyle: inlineStyle,
				justUpdated: true
			});

			//	Back To Normal
			//--------------------------------------------------------//
			setTimeout(() => {
				this.setState({justUpdated: false});
			}, 500);
		}
	}

	//	Ajax Load
	//--------------------------------------------------------//
	ajaxLoad = async (e) => {
		if(e !== undefined) { e.preventDefault(); }
		this.setState({
			isLoading: true,
			isLoaded: false
		});
		try {
			//	Get Values
			//--------------------------------------------------------//
			await Promise.all(this.props.entities.map(async (entity, entityIndex) => {
				const res = await axios.get('/api/states/'+entity);
				this.update(res.data);
			}));
			this.setState({
				isLoading: false,
				isLoaded: true,
				hasLoaded: true,
				isError: false
			});
		} catch(error) {
			this.setState({
				isLoading: false,
				isLoaded: false,
				isError: true,
				hasErrored: true
			});
			console.error(error);
		}
	}

	//	Render
	//--------------------------------------------------------//
	render() {
		return (
			<Box className={this.tileClasses} {...this.props} pose={this.state.justUpdated ? 'justUpdated' : 'normal'}>
				<Card>
					<CardHeader>{this.props.meta || this.state.meta}</CardHeader>
					<CardBody className={this.state.colorClasses} style={this.state.inlineStyle}>
						{ this.state.icon ? (
							<i className={this.state.iconClasses}></i>
						) : (
							this.props.subtype === 'gauge' ? (
								<div>
									<div className="gauge"><Doughnut data={this.state.gaugeData} options={this.graphOptions} /></div>
									<div className="stat"><CountUp start={this.state.oldValue} end={this.state.value} decimals={this.props.precision} /></div>
								</div>
							) : this.props.subtype === 'percentage' ? (
								<CircularProgressbar strokeWidth={this.props.percentWidth} percentage={this.state.percentValue} text={`${this.state.percentValue}%`} />
							) : (
								<div className="stat"><CountUp start={this.state.oldValue} end={this.state.value} decimals={this.props.precision} /></div>
							)
						)}
					</CardBody>
					<CardFooter>
						<span className="label">{this.props.label || this.state.label}</span>
						{ this.props.entities.length > 1 ? (
								<span className="badge badge-secondary">{this.props.entities.length}</span>
							) : null
						}
					</CardFooter>
				</Card>
			</Box>
	    );
	}
}
