import React from 'react';
import { Card, CardImgOverlay, CardFooter } from 'reactstrap';
import axios from 'axios';
import classNames from 'classnames';
import tile from '../../common/tile.js';
import posed from 'react-pose';
import { get } from 'lodash';

//--------------------------------------------------------//
//	Pose
//--------------------------------------------------------//
const Box = posed.div(tile.pose());

//--------------------------------------------------------//
//	Component
//--------------------------------------------------------//
export default class TileImage extends React.Component {

	//	Default Props
	//--------------------------------------------------------//
	static defaultProps = {
		type: 'line',
		x: 0, y: 0, w: 1, h: 1,
		label: '',
		meta: '',
		entities: [],
		interval: 30,
		source: 'attributes.entity_picture',
		ws: {}
	};

	//	Constructor
	//--------------------------------------------------------//
	constructor(props, defaultProps) {
		super(props, defaultProps);
		this.state = {
			isLoading: false,
			isLoaded: false,
			hasLoaded: false,
			isError: false,
			hasErrored: false,

			src: '',
			label: undefined,
			meta: undefined
		};
	}

	//	Handlers
	//--------------------------------------------------------//
	timerID;

	//	Lifecycle
	//--------------------------------------------------------//
	componentDidMount() {
		this.ajaxLoad();

		//	Ajax Refresh
		//--------------------------------------------------------//
		if(this.props.ajaxInterval > 0) {
			this.timerID = setInterval(() => this.ajaxLoad(), this.props.ajaxInterval * 1000);
		}
	}

	componentWillUnmount() { clearInterval(this.timerID); }

	static getDerivedStateFromProps(props, state) {
		if(get(props, 'ws.event.event_type', '') === 'state_changed' && props.entities.indexOf(props.ws.event.data.entity_id) > -1) {
			return { rawData: props.ws.event.data.new_state };
		} else { return null; }
	}

	componentDidUpdate(props, state) {
		if(get(state, 'rawData.state', '') === '') { return; }
		else if(state.value !== state.rawData.state) {
			this.update(state.rawData);
		}
	}

	//	Classes
	//--------------------------------------------------------//
	tileClasses = classNames(tile.classes(this.props));

	//	Update
	//--------------------------------------------------------//
	update = (data) => {
		const value = get(data, this.props.source);
		if(this.state.value !== value) {
			this.setState({
				rawData: {},
				value: value,
				label: data.attributes.friendly_name,
				justUpdated: true
			});

			//	Back To Normal
			//--------------------------------------------------------//
			setTimeout(() => {
				this.setState({justUpdated: false});
			}, 500);
		}
	}

	//	Ajax Load
	//--------------------------------------------------------//
	ajaxLoad = async (e) => {
		if(e !== undefined) { e.preventDefault(); }
		this.setState({
			isLoading: true,
			isLoaded: false
		});
		try {
			const res = await axios.get('/api/states/'+this.props.entities[0]);
			this.setState({
				isLoading: false,
				isLoaded: true,
				hasLoaded: true,
				isError: false
			});
			this.update(res.data);
		} catch(error) {
			this.setState({
				isLoading: false,
				isLoaded: false,
				isError: true,
				hasErrored: true
			});
			console.error(error);
		}
	}

	//	Render
	//--------------------------------------------------------//
	render() {
		return (
			<Box className={this.tileClasses} {...this.props} pose={this.state.justUpdated ? 'justUpdated' : 'normal'}>
				<Card style={{backgroundImage: `url(${this.state.value})`}}>
					<CardImgOverlay>
						<CardFooter>
							<span className="label">{this.props.label || this.state.label}</span>
						</CardFooter>
					</CardImgOverlay>
				</Card>
			</Box>
	    );
	}
}
