import React from 'react';
import { Card, CardHeader, CardBody, CardFooter } from 'reactstrap';
import Switch from 'rc-switch';
import Slider from 'rc-slider';
import axios from 'axios';
import classNames from 'classnames';
import tile from '../../common/tile.js';
import process from '../../common/process.js';
import moment from 'moment';
import posed from 'react-pose';
import { get, round } from 'lodash';

//--------------------------------------------------------//
//	Pose
//--------------------------------------------------------//
const Box = posed.div(tile.pose());

//--------------------------------------------------------//
//	Component
//--------------------------------------------------------//
export default class TileSwitch extends React.Component {

	//	Default Props
	//--------------------------------------------------------//
	static defaultProps = {
		type: 'switch',
		subtype: 'switch',
		x: 0, y: 0, w: 1, h: 1,
		entities: [],
		label: '',
		meta: '',
		metaType: 'STRING',
		valueType: 'MIXED',
		domain: 'light',
		ajaxInterval: 0,
		icons: {},
		colors: {},
		step: 10,
		ws: {}
	};

	//	Constructor
	//--------------------------------------------------------//
	constructor(props, defaultProps) {
		super(props, defaultProps);
		this.state = {
			isLoading: false,
			isLoaded: false,
			hasLoaded: false,
			isSaving: false,
			isSaved: false,
			hasSaved: false,
			isError: false,
			hasErrored: false,
			rawData: {},
			entityValues: [],
			entityBrightness: [],
			value: '',
			brightness: 50,
			oldValue: '',
			icon: false,
			iconClasses: '',
			color: false,
			colorClasses: ''
		};
	}

	//	Handlers
	//--------------------------------------------------------//
	timerID;

	//	Lifecycle
	//--------------------------------------------------------//
	componentDidMount() {
		this.ajaxLoad();

		//	Ajax Refresh
		//--------------------------------------------------------//
		if(this.props.ajaxInterval > 0) {
			this.timerID = setInterval(() => this.ajaxLoad(), this.props.ajaxInterval * 1000);
		}
	}

	componentWillUnmount() { clearInterval(this.timerID); }

	static getDerivedStateFromProps(props, state) {
		if(get(props, 'ws.event.event_type', '') === 'state_changed' && props.entities.indexOf(props.ws.event.data.entity_id) > -1) {
			return { rawData: props.ws.event.data.new_state };
		} else { return null; }
	}

	componentDidUpdate(props, state) {
		if(get(state, 'rawData.state', '') === '') { return; }
		else { this.update(get(state, 'rawData')); }
	}

	//	Classes
	//--------------------------------------------------------//
	tileClasses = classNames(tile.classes(this.props));

	//	Update
	//--------------------------------------------------------//
	update = (data) => {

		//	Setup
		//--------------------------------------------------------//
		let label = this.state.label;
		let meta = this.state.meta;
		let entityValues = this.state.entityValues;
		let entityBrightness = this.state.entityBrightness;
		const oldValue = this.state.value;
		const oldBrightness = this.state.brightness;
		const entityIndex = this.props.entities.indexOf(data.entity_id);
		if(entityIndex < 0) { return; }

		//	Label & Meta
		//--------------------------------------------------------//
		if(label === undefined) { label = get(data, 'attributes.friendly_name'); }
		switch(this.props.metaType) {
			case 'UPDATED':
				meta = moment(get(data, 'last_changed')).format('Do MMM HH:mm');
				break;
			default:
				meta = meta ? meta : '';
				break;
		}

		//	Value
		//--------------------------------------------------------//
		entityValues[entityIndex] = data.state;
		entityBrightness[entityIndex] = round((get(data, 'attributes.brightness', process.toBool(data.state) ? 255 : 0) / 255) * 100);

		//	Processed Value
		//--------------------------------------------------------//
		const value = process.toBool(process.string(entityValues, this.props));
		const brightness = process.string(entityBrightness, {valueType: 'AVG'});
		const icon = process.icon(value, this.props);

		let iconClasses = '';
		if(icon) {
			iconClasses = classNames({
				'mdi': true,
				[`mdi-${icon}`]: true
			});
		}

		const color = process.color(value, this.props);
		const colorClasses = classNames({
			'color': (color !== false),
			'color--vanilla': (color === false),
			[`color--${color}`]: true
		});

		//	Save Update
		//--------------------------------------------------------//
		if(value !== oldValue || brightness !== oldBrightness) {
			this.setState({
				rawData: {},
				label: label,
				meta: meta,
				value: value,
				oldValue: oldValue,
				brightness: brightness,
				sliderValue: brightness,
				oldBrightness: oldBrightness,
				icon: icon,
				iconClasses: iconClasses,
				color: color,
				colorClasses: colorClasses,
				justUpdated: true
			});

			//	Back To Normal
			//--------------------------------------------------------//
			setTimeout(() => {
				this.setState({justUpdated: false});
			}, 500);
		}
	}

	//	Ajax Load
	//--------------------------------------------------------//
	ajaxLoad = async (e) => {
		if(e !== undefined) { e.preventDefault(); }
		this.setState({
			isLoading: true,
			isLoaded: false
		});
		try {
			await Promise.all(this.props.entities.map(async (entity, entityIndex) => {
				const res = await axios.get('/api/states/'+entity);
				this.update(res.data);
			}));
			this.setState({
				isLoading: false,
				isLoaded: true,
				hasLoaded: true,
				isError: false
			});
		} catch(error) {
			this.setState({
				isLoading: false,
				isLoaded: false,
				isError: true,
				hasErrored: true
			});
			console.error(error);
		}
	}

	//	Toggle
	//--------------------------------------------------------//
	toggle = () => {
		const current = this.state.value;
		const service = current ? 'turn_off' : 'turn_on';
		this.save(service, 100);
	}

	//	Set Value
	//--------------------------------------------------------//
	setValue = (brightness) => {
		const service = (brightness > 0) ? 'turn_on' : 'turn_off';
		this.save(service, brightness);
	}

	//	Set Value
	//--------------------------------------------------------//
	slide = (value) => {
		this.setState({ sliderValue: value });
	}

	//	 Save
	//--------------------------------------------------------//
	save = async (service, brightness) => {
		this.setState({
			isSaving: true,
			isSaved: false
		});
		try {
			await Promise.all(this.props.entities.map(async (entity, entityIndex) => {
				let body = { entity_id: entity };
				if(service === 'turn_on') { body.brightness_pct = brightness; }
				const res = await axios.post(`/api/services/${this.props.domain}/${service}`, body);
				res.data.map(entity => this.update(entity));
			}));
			this.setState({
				isSaving: false,
				isSaved: true,
				hasSaved: true,
				isError: false
			});
		} catch(error) {
			this.setState({
				isSaving: false,
				isSaved: false,
				isError: true,
				hasErrored: true
			});
			console.error(error);
		}
	}

	//	Render
	//--------------------------------------------------------//
	render() {
		return (
			<Box className={this.tileClasses} {...this.props} pose={this.state.justUpdated ? 'justUpdated' : 'normal'}>
				<Card>
					<CardHeader>{this.props.meta || this.state.meta}</CardHeader>
					<CardBody className={this.state.colorClasses}>
						{ this.props.subtype === 'switch' ? ( <Switch className="_centered" checked={this.state.value} onClick={this.toggle} /> ) : null }
						{ this.props.subtype === 'dimmer' ? ( <Slider className="_centered" vertical disabled={false} included={true} step={this.props.step} value={this.state.sliderValue} onChange={this.slide} onAfterChange={this.setValue} /> ) : null }
					</CardBody>
					<CardFooter>
						<span className="label">{this.props.label || this.state.label}</span>
						{ this.props.entities.length > 1 ? ( <span className="badge badge-secondary">{this.props.entities.length}</span> ) : null }
					</CardFooter>
				</Card>
			</Box>
	    );
	}
}
