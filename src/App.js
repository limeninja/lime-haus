import React, { Component } from 'react';
import TileDarkSky from './components/TileDarkSky';
import TileImage from './components/TileImage';
import TileLine from './components/TileLine';
import TileNumber from './components/TileNumber';
import TileString from './components/TileString';
import TileSwitch from './components/TileSwitch';
import yaml from 'js-yaml';
import axios from 'axios';
import Websocket from 'react-websocket';

//--------------------------------------------------------//
//	Component
//--------------------------------------------------------//
export default class App extends Component {

	//	Default Props
	//--------------------------------------------------------//
	static defaultProps = {};

	//	Constructor
	//--------------------------------------------------------//
	constructor(props, defaultProps) {
		super(props, defaultProps);
		this.state = {
			isLoading: false,
			isLoaded: false,
			hasLoaded: false,
			isError: false,
			hasErrored: false,
			ws: {}
		};
	}

	//	Lifecycle
	//--------------------------------------------------------//
	componentDidMount() { this.loadData(); }
	componentWillUnmount() {}

	//	Sockets
	//--------------------------------------------------------//
	wsData = (data) => {
		const res = JSON.parse(data);
		//console.log('handleData', res);
		switch(res.type) {
			case 'auth_required':
				this.wsAuth();
				break;
			case 'auth_ok':
				this.wsSubscribe();
				break;
			default:
				this.setState({ ws: res });
				break;
		}
	}

	wsAuth = () => {
		this.wsSend({
			type: 'auth',
			access_token: this.state.data.haus.longLivedAccessToken
		});
	}

	wsSubscribe = () => {
		this.wsSend({
			id: 1,
			type: 'subscribe_events'
		});
	}

	wsSend = (message) => {
		this.ws.sendMessage(JSON.stringify(message));
	}

	//	Functions
	//--------------------------------------------------------//
	loadData = async (e) => {
		if(e !== undefined) { e.preventDefault(); }
		this.setState({
			isLoading: true,
			isLoaded: false
		});

		//	Load Locked Config
		//--------------------------------------------------------//
		try {
			const lockedConfig = await axios.get('/local/lime.haus.yml');
			const config = yaml.safeLoad(lockedConfig.data);

			//	Set API Key
			//--------------------------------------------------------//
			axios.defaults.headers.common['Authorization'] = `Bearer ${config.haus.longLivedAccessToken}`;

			//	Styling
			//--------------------------------------------------------//
			if(config.style instanceof Object) {
				let root = document.documentElement;
				Object.keys(config.style).forEach(function(key) {
					root.style.setProperty(`--${key}`, config.style[key]);
				});
			}

			//	Save
			//--------------------------------------------------------//
			this.setState({
				isLoading: false,
				isLoaded: true,
				hasLoaded: true,
				isError: false,
				data: config
			});
		} catch(error) {
			this.setState({
				isLoading: false,
				isLoaded: false,
				isError: true,
				hasErrored: true
			});
			console.error(error);
		}
	}

	render() {
		return (
			<main>
				{ this.state.isLoaded ? (
					<div>
						{ this.state.data.views[0].tiles.map((props, tileKey) => (
							<div>
								{ props.type === 'darksky' ? ( <TileDarkSky {...props}  /> ) : null }
								{ props.type === 'image' ? ( <TileImage {...props} ws={this.state.ws} /> ) : null }
								{ props.type === 'line' ? ( <TileLine {...props}  /> ) : null }
								{ props.type === 'number' ? ( <TileNumber {...props} ws={this.state.ws} /> ) : null }
								{ props.type === 'string' ? ( <TileString {...props} ws={this.state.ws} /> ) : null }
								{ props.type === 'switch' ? ( <TileSwitch {...props} ws={this.state.ws} /> ) : null }
							</div>
						))}
						<Websocket url={`ws://${window.location.hostname}:${window.location.port}/api/websocket`}
	    					onMessage={this.wsData}
	    					reconnect={true}
	    					debug={true}
	    					ref={Websocket => {this.ws = Websocket;}}
	    				/>
					</div>
				 ) : null }
			</main>
	    );
	}
}
