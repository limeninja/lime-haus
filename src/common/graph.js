export default {

	//--------------------------------------------------------//
	//	Gauge Options
	//--------------------------------------------------------//
	gaugeOptions: (props) => {
		return {
			maintainAspectRatio: false,
			circumference: Math.PI,
        	rotation : Math.PI,
        	cutoutPercentage : 30,
			tooltips: { enabled: false },
			hover: { mode: null }
		}
	},

	//--------------------------------------------------------//
	//	Gauge Seed Dataset
	//--------------------------------------------------------//
	gaugeDataset: (props) => {
		return {
			label: '',
			meta: '',
			data: [],
			backgroundColor: [],
			borderWidth: 0
		}
	},

	//--------------------------------------------------------//
	//	Line Options
	//--------------------------------------------------------//
	lineOptions: (props) => {
		return {
			spanGaps: props.spanGaps,
			maintainAspectRatio: false,
			legend: { display: false },
			tooltips: {
				enabled: true,
				mode: 'x'
			},
			scales: {
				xAxes: [{
					display: true,
					ticks: { display: false },
					gridLines: { display : false }
				}],
				yAxes: [{
					ticks: {
						precision: 1
					}
				}]
			}
		}
	},

	//--------------------------------------------------------//
	//	Line Seed Dataset
	//--------------------------------------------------------//
	lineDataset: (props) => {
		return {
			label: '',
			meta: '',
			data: [],
			fill: false,
			lineTension: props.lineTension,
			borderWidth: props.borderWidth,
			pointRadius: props.pointRadius,
			pointHitRadius: props.pointHitRadius
		}
	}
}
