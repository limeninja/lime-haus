import React from 'react';
import { Card, CardHeader, CardBody, CardFooter } from 'reactstrap';
import { Line } from 'react-chartjs-2';
import axios from 'axios';
import classNames from 'classnames';
import tile from '../../common/tile.js';
import graph from '../../common/graph.js';
import process from '../../common/process.js';
import { extendMoment } from 'moment-range';
import { cloneDeep, round } from 'lodash';
import posed from 'react-pose';
import Moment from 'moment';
const moment = extendMoment(Moment);

//--------------------------------------------------------//
//	Pose
//--------------------------------------------------------//
const Box = posed.div(tile.pose());

//--------------------------------------------------------//
//	Component
//--------------------------------------------------------//
export default class TileLine extends React.Component {

	//	Default Props
	//--------------------------------------------------------//
	static defaultProps = {
		type: 'line',
		subtype: 'line',
		x: 0, y: 0, w: 1, h: 1,
		entities: [],
		label: '',
		meta: '',
		precision: 1,
		valueType: 'AVG',
		metaType: 'STRING',
		ajaxInterval: 30,

		history: 24,															// Hours
		resolution: 60,															// Minutes
		spanGaps: true,
		lineTension: 0.4,
		borderWidth: 3,
		pointRadius: 3,
		pointHitRadius: 10
	};

	//	Constructor
	//--------------------------------------------------------//
	constructor(props, defaultProps) {
		super(props, defaultProps);
		this.state = {
			isLoading: false,
			isLoaded: false,
			hasLoaded: false,
			isError: false,
			hasErrored: false,

			graphData: {}
		};

		//	Chart Setup
		//--------------------------------------------------------//
		this.graphOptions = graph.lineOptions(props);
		this.seedDataset = graph.lineDataset(props);
	}

	//	Handlers
	//--------------------------------------------------------//
	timerID;

	//	Lifecycle
	//--------------------------------------------------------//
	componentDidMount() {
		this.ajaxLoad();

		//	Ajax Refresh
		//--------------------------------------------------------//
		if(this.props.ajaxInterval > 0) {
			this.timerID = setInterval(() => this.ajaxLoad(), this.props.ajaxInterval * 1000);
		}
	}
	componentWillUnmount() { clearInterval(this.timerID); }

	//	Classes
	//--------------------------------------------------------//
	tileClasses = classNames(tile.classes(this.props));

	//	Root Style
	//--------------------------------------------------------//
	rootStyle = getComputedStyle(document.body);

	//	Ajax Load
	//--------------------------------------------------------//
	ajaxLoad = async (e) => {
		if(e !== undefined) { e.preventDefault(); }
		this.setState({
			isLoading: true,
			isLoaded: false
		});
		try {

			//	Windows Slices
			//--------------------------------------------------------//
			const resolutionMinutes = this.props.resolution;
			const roundedNow = Math.round(moment().minute() / resolutionMinutes) * resolutionMinutes;
			const windowEnd = moment().minute(roundedNow).second(0);
			const windowStart = moment(windowEnd).subtract(this.props.history, 'hours');
			const windowRange = moment.range(windowStart, windowEnd);

			let temp = {};
			Array.from(windowRange.by('minutes', {step: resolutionMinutes})).map((v, i) => temp[v.format('YYYYMMDDHHmm')] = null);
			const windowSlices = temp;

			//	Get Data
			//--------------------------------------------------------//
			const res = await axios.get('/api/history/period/'+moment().subtract(this.props.history, 'hours').format('YYYY-MM-DDTHH:mm:ssZ'), {
				params: {
					filter_entity_id: this.props.entities.join(','),
					end_time: moment().format('YYYY-MM-DDTHH:mm:ssZ')
				}
			});

			//	Build Data Object
			//--------------------------------------------------------//
			let rawData = [];
			this.props.entities.forEach((entity, entityIndex) => {
				rawData[entityIndex] = cloneDeep(windowSlices);
				res.data[entityIndex].forEach((entityData, entityDataIndex) => {
					const timestamp = moment(entityData.last_updated);
					const roundedSlice = Math.round(timestamp.minute() / resolutionMinutes) * resolutionMinutes;
					const slice = timestamp.minute(roundedSlice).second(0).format('YYYYMMDDHHmm');

					if(rawData[entityIndex][slice] === null) { rawData[entityIndex][slice] = []; }

					const value = parseFloat(entityData.state);
					if(!isNaN(value)) { rawData[entityIndex][slice].push(parseFloat(entityData.state)); }
				});
			});

			//	Process Data
			//--------------------------------------------------------//
			rawData.forEach((entity, enityIndex) => {
				Object.keys(entity).forEach(datasetIndex => {
					if(rawData[enityIndex][datasetIndex] === null) { rawData[enityIndex][datasetIndex] = null; }
					else { rawData[enityIndex][datasetIndex] = round(process.numbers(rawData[enityIndex][datasetIndex], this.props), this.props.precision);}
				});
			});

			//	Colours
			//--------------------------------------------------------//
			const style = getComputedStyle(document.body);
			const colors = [
				style.getPropertyValue('--c-1'),
				style.getPropertyValue('--c-2'),
				style.getPropertyValue('--c-3'),
				style.getPropertyValue('--c-4'),
				style.getPropertyValue('--c-5'),
				style.getPropertyValue('--c-6'),
				style.getPropertyValue('--c-7'),
				style.getPropertyValue('--c-8')
			];

			//	Format Data
			//--------------------------------------------------------//
			let data = {
				labels: [],
				datasets: []
			};
			rawData.forEach((entity, entityIndex) => {
				let datasetObject = cloneDeep(this.seedDataset);
				datasetObject.borderColor = colors[entityIndex];
				datasetObject.backgroundColor = colors[entityIndex];
				datasetObject.label = res.data[entityIndex][0].attributes.friendly_name;
				datasetObject.meta = res.data[entityIndex][0].attributes.unit_of_measurement;

				Object.keys(entity).forEach(datasetIndex => {
					if(entityIndex === 0) { data.labels.push(moment(datasetIndex, 'YYYYMMDDHHmm').format('HH:mm')); }
					datasetObject.data.push(rawData[entityIndex][datasetIndex]);
				});
				data.datasets.push(datasetObject);
			});

			//	Update State
			//--------------------------------------------------------//
			this.setState({
				isLoading: false,
				isLoaded: true,
				hasLoaded: true,
				isError: false,

				data: data,
				label: data.datasets[0].label,
				meta: data.datasets[0].meta,
				justUpdated: true
			});

			//	Back To Normal
			//--------------------------------------------------------//
			setTimeout(() => {
				this.setState({justUpdated: false});
			}, 500);

		} catch(error) {
			this.setState({
				isLoading: false,
				isLoaded: false,
				isError: true,
				hasErrored: true
			});
			console.error(error);
		}
	}

	//	Render
	//--------------------------------------------------------//
	render() {
		return (
			<Box className={this.tileClasses} {...this.props} pose={this.state.justUpdated ? 'justUpdated' : 'normal'}>
				<Card>
					<CardHeader>{this.props.meta || this.state.meta}</CardHeader>
					<CardBody>
						<Line data={this.state.data} options={this.graphOptions} />
					</CardBody>
					<CardFooter>
						<span className="label">{this.props.label || this.state.label}</span>
						{ this.props.entities.length > 1 ? (
								<span className="badge badge-secondary">{this.props.entities.length}</span>
							) :null
						}
					</CardFooter>
				</Card>
			</Box>
	    );
	}
}
